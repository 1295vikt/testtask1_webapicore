﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiCoreBL.ModelsBL
{
    public class GenericPagedList<ModelBL>
        where ModelBL : class
    {
        public IEnumerable<ModelBL> Items { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
    }
}
