﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiCoreBL.ModelsBL
{
    public class UmbracoCompanyRequest
    {
        public string CompanyName { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string Comment { get; set; }
        public string LogoImageURL { get; set; }
    }

}
