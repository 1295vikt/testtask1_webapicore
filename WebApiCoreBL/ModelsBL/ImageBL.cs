﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiCoreBL.ModelsBL
{
    public class ImageBL
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string ImageURL { get; set; }
    }
}
