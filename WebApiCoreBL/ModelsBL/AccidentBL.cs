﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiCoreBL.ModelsBL
{
    public class AccidentBL
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string AccidentAddress { get; set; }
        public DateTime AccidentDate { get; set; }
        public virtual IEnumerable<CarBL> Cars { get; set; }
        public virtual IEnumerable<DamageBL> Damages { get; set; }
        public virtual IEnumerable<ImageBL> Images { get; set; }
    }
}
