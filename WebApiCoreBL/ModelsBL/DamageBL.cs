﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiCoreBL.ModelsBL
{
    public class DamageBL
    {
        public int Id { get; set; }
        public int CarId { get; set; }
        public int? AccidentId { get; set; }

        public string DamageDescription { get; set; }
        public int EstimatedRepairCostUSD { get; set; }
        public bool IsRepaired { get; set; }

        public CarBL Car { get; set; }
        public AccidentBL Accident { get; set; }
        public IEnumerable<ImageBL> Images { get; set; }
    }
}
