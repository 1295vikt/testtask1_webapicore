﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiCoreBL.ModelsBL
{
    public class CarBL
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string Model { get; set; }
        public string BodyType { get; set; }
        public int ColorId { get; set; }
        public int GearboxTypeId { get; set; }
        public string FuelType { get; set; }
        public int ManufactureYear { get; set; }
        public double Mileage { get; set; }
        public string DriverName { get; set; }
        public string DriverContactPhone { get; set; }

        public IEnumerable<ImageBL> Images { get; set; }
        public IEnumerable<AccidentBL> Accidents { get; set; }
        public IEnumerable<DamageBL> Damages { get; set; }
    }
}
