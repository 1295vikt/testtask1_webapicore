﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using WebApiCoreBL.ModelsBL;
using WebApiCoreDAL.Entities;

namespace WebApiCoreBL.Configs
{
    public class BLAutomapperProfile : Profile
    {
        public BLAutomapperProfile()
        {

            CreateMap<Car, CarBL>()
                .ForMember(dest => dest.Damages, opt => opt.Ignore())
                .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.ImageLinks.Select(x => x.Image)))
                .AfterMap((src, dest) =>
                {
                    if (src.AccidentLinks != null)
                    {
                        var accidents = src.AccidentLinks.Select(x => x.Accident);
                        dest.Accidents = MapAccidenListNoProxies(accidents);
                    }

                    if (src.Damages != null)
                    {
                        dest.Damages = MapDamageListNoProxies(src.Damages);
                    }

                });
            CreateMap<CarBL, Car>().ForMember(dest => dest.ImageLinks, opt => opt.MapFrom(src => src.Images));

            CreateMap<Accident, AccidentBL>()
                .ForMember(dest => dest.Damages, opt => opt.Ignore())
                .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.ImageLinks.Select(x => x.Image)))
                .AfterMap((src, dest) =>
                {
                    if (src.Damages != null)
                    {
                        dest.Damages = MapDamageListNoProxies(src.Damages);
                    }

                    if (src.CarLinks != null)
                    {
                        var cars = src.CarLinks.Select(x => x.Car);
                        dest.Cars = MapCarListNoProxies(cars);
                    }
                });
            CreateMap<AccidentBL, Accident>()
                .AfterMap((src, dest) =>
                {
                    dest.ImageLinks = src.Images.Select(x => new ImageAccident
                    {
                        ImageId = x.Id,
                        Image = new Image
                        {
                            Id = x.Id,
                            Description = x.Description,
                            ImageURL = x.ImageURL
                        }
                    }).ToList();
                });



            CreateMap<Damage, DamageBL>()
                .ForMember(dest => dest.Car, opt => opt.Ignore())
                .ForMember(dest => dest.Accident, opt => opt.Ignore())
                .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.ImageLinks.Select(x => x.Image)))
                .AfterMap((src, dest) =>
                {
                    if (src.Car != null)
                    {
                        dest.Car = MapCarNoProxies(src.Car);
                    }
                    if (src.Accident != null)
                    {
                        dest.Accident = MapAccidentNoProxies(src.Accident);
                    }
                });
            CreateMap<DamageBL, Damage>()
                .AfterMap((src, dest) =>
                {
                    dest.ImageLinks = src.Images.Select(x => new ImageDamage
                    {
                        ImageId = x.Id,
                        Image = new Image
                        {
                            Id = x.Id,
                            Description = x.Description,
                            ImageURL = x.ImageURL
                        }
                    }).ToList();
                });

            CreateMap<ImageBL, Image>().ReverseMap();

            CreateMap<ImageBL, ImageCar>().ForMember(dest => dest.Image, opt => opt.MapFrom(src => src));
            CreateMap<ImageBL, ImageAccident>().ForMember(dest => dest.Image, opt => opt.MapFrom(src => src));
            CreateMap<ImageBL, ImageDamage>().ForMember(dest => dest.Image, opt => opt.MapFrom(src => src));

        }


        private CarBL MapCarNoProxies(Car car)
        {
            var carBL = new CarBL
            {
                Id = car.Id,
                CompanyId = car.CompanyId,
                Model = car.Model,
                BodyType = car.BodyType,
                FuelType = car.FuelType,
                GearboxTypeId = car.GearboxTypeId,
                ColorId = car.ColorId,
                DriverName = car.DriverName,
                DriverContactPhone = car.DriverContactPhone,
                ManufactureYear = car.ManufactureYear,
                Mileage = car.Mileage
            };
            return carBL;
        }

        private IEnumerable<CarBL> MapCarListNoProxies(IEnumerable<Car> cars)
        {
            return cars.Select(x => MapCarNoProxies(x));
        }

        private AccidentBL MapAccidentNoProxies(Accident accident)
        {
            var accidentBL = new AccidentBL
            {
                Id = accident.Id,
                AccidentAddress = accident.AccidentAddress,
                AccidentDate = accident.AccidentDate,
                Description = accident.Description,
                Images = accident.ImageLinks.Select(x => new ImageBL
                {
                    Id = x.ImageId,
                    ImageURL = x.Image.ImageURL,
                    Description = x.Image.Description
                })
            };
            return accidentBL;
        }

        private IEnumerable<AccidentBL> MapAccidenListNoProxies(IEnumerable<Accident> accidents)
        {
            return accidents.Select(x => MapAccidentNoProxies(x));
        }


        private DamageBL MapDamageNoProxies(Damage damage)
        {
            var damageBL = new DamageBL
            {
                Id = damage.Id,
                AccidentId = damage.AccidentId,
                CarId = damage.CarId,
                DamageDescription = damage.DamageDescription,
                EstimatedRepairCostUSD = damage.EstimatedRepairCostUSD,
                IsRepaired = damage.IsRepaired,
                Images = damage.ImageLinks.Select(x => new ImageBL
                {
                    Id = x.ImageId,
                    ImageURL = x.Image.ImageURL,
                    Description = x.Image.Description
                })
            };
            return damageBL;
        }

        private IEnumerable<DamageBL> MapDamageListNoProxies(IEnumerable<Damage> damages)
        {
            return damages.Select(x => MapDamageNoProxies(x));
        }


    }

}