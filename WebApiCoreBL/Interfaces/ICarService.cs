﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApiCoreBL.ModelsBL;

namespace WebApiCoreBL.Interfaces
{
    public interface ICarService : IGenereicService<CarBL>
    {
        Task<IEnumerable<CarBL>> GetByCompanyId(int companyId);

    }
}
