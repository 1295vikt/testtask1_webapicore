﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebApiCoreBL.ModelsBL;

namespace WebApiCoreBL.Interfaces
{
    public interface IGenereicService<ModelBL>
     where ModelBL : class
    {
        Task<ModelBL> FindById(int id);
        Task<ModelBL> Create(ModelBL modelBL);
        Task Delete(int id);
        Task<ModelBL> Update(int id, ModelBL modelBL);
        Task<IEnumerable<ModelBL>> GetAll();
        Task<GenericPagedList<ModelBL>> GetPaged(int itemsPerPage, int Page, params Expression<Func<ModelBL, bool>>[] filters);

        public Task<bool> CheckIfExists(int id);
    }
}
