﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApiCoreBL.ModelsBL;

namespace WebApiCoreBL.Interfaces
{
    public interface IUmbracoService
    {
        Task<IEnumerable<UmbracoCompanyResponse>> GetCompanies();
        Task<UmbracoCompanyResponse> GetCompanyById(int id);
        Task<UmbracoCompanyResponse> CreateCompany(UmbracoCompanyRequest companyReq);
        Task<UmbracoCompanyResponse> UpdateCompany(int id, UmbracoCompanyRequest companyReq);
        Task<IDictionary<int, string>> GetCompanyDictionary();
        Task DeleteCompany(int id);

        Task<IDictionary<int, string>> GetColorDictionary();
        Task<string> GetColorName(int id);

    }
}
