﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using WebApiCoreBL.Configs;
using WebApiCoreDAL;
using WebApiCoreDAL.Interfaces;
using WebApiCoreDAL.Repositories;

namespace WebApiCoreBL
{
    public static class BLServices
    {
        public static void AddServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<CarsContext>(options => options.UseSqlServer(configuration.GetConnectionString("CarsDbConnectionString")));
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddSingleton(typeof(ICarsDbInitializer), typeof(CarsDbInitializer));
            services.AddSingleton(typeof(ICarsDbInitializer), typeof(CarsDbInitializer));

            UmbracoApiConfiguration apiConfiguration = new UmbracoApiConfiguration();
            configuration.GetSection("UmbracoApiConfiguration").Bind(apiConfiguration);
            services.AddSingleton(apiConfiguration);

        }
    }
}
