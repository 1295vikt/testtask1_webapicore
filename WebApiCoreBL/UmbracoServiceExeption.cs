﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace WebApiCoreBL
{
    public class UmbracoServiceExeption : Exception
    {
        public UmbracoServiceExeption(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }
        
        public HttpStatusCode StatusCode { get; private set; }

    }
}
