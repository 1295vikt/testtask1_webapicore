﻿using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebApiCoreBL.Configs;
using WebApiCoreBL.Interfaces;
using WebApiCoreBL.ModelsBL;

namespace WebApiCoreBL.Services
{
    public class UmbracoService : IUmbracoService
    {

        private readonly RestClient _client;
        private readonly IMemoryCache _cache;

        private const string companiesEndpoint = "/companies/{id}";
        private const string colorsEndpoint = "/colors/{id}";

        public UmbracoService(UmbracoApiConfiguration configuration, IMemoryCache cache)
        {
            _client = new RestClient(configuration.BaseURL);
            _cache = cache;
        }

        public async Task<IEnumerable<UmbracoCompanyResponse>> GetCompanies()
        {

            var companiesCacheKey = "Companies";
            IEnumerable<UmbracoCompanyResponse> response;
            if (!_cache.TryGetValue(companiesCacheKey, out response))
            {
                var request = new RestRequest(companiesEndpoint, Method.GET);
                request.AddUrlSegment("id", "");

                var restResponse = await _client.ExecuteAsync<IEnumerable<UmbracoCompanyResponse>>(request);
                if (restResponse.IsSuccessful)
                {
                    response = restResponse.Data;
                    _cache.Set(companiesCacheKey, response);
                }
                else
                {
                    throw new UmbracoServiceExeption(restResponse.StatusCode, restResponse.Content);
                }
            }

            return response;
        }

        public async Task<UmbracoCompanyResponse> GetCompanyById(int id)
        {

            var companyCacheKey = $"Company{id}";
            UmbracoCompanyResponse response;
            if (!_cache.TryGetValue(companyCacheKey, out response))
            {
                var request = new RestRequest(companiesEndpoint, Method.GET);
                request.AddUrlSegment("id", id);

                var restResponse = await _client.ExecuteAsync<UmbracoCompanyResponse>(request);
                if (restResponse.IsSuccessful)
                {
                    response = restResponse.Data;
                    _cache.Set(companyCacheKey, response);
                }
                else
                {
                    throw new UmbracoServiceExeption(restResponse.StatusCode, restResponse.Content);
                }
            }

            return response;
        }

        public async Task<UmbracoCompanyResponse> CreateCompany(UmbracoCompanyRequest companyReq)
        {

            var request = new RestRequest(companiesEndpoint, Method.POST);
            request.AddUrlSegment("id", "");
            request.AddObject(companyReq);

            var restResponse = await _client.ExecuteAsync<UmbracoCompanyResponse>(request);
            if (!restResponse.IsSuccessful)
            {
                throw new UmbracoServiceExeption(restResponse.StatusCode, restResponse.Content);
            }

            _cache.Remove("Companies");

            var response = restResponse.Data;

            return response;
        }

        public async Task<UmbracoCompanyResponse> UpdateCompany(int id, UmbracoCompanyRequest companyReq)
        {

            var request = new RestRequest(companiesEndpoint, Method.PUT);
            request.AddUrlSegment("id", id);
            request.AddObject(companyReq);

            var restResponse = await _client.ExecuteAsync<UmbracoCompanyResponse>(request);
            if (!restResponse.IsSuccessful)
            {
                throw new UmbracoServiceExeption(restResponse.StatusCode, restResponse.Content);
            }

            _cache.Remove("Companies");
            _cache.Remove($"Company{id}");

            var response = restResponse.Data;

            return response;
        }

        public async Task<IDictionary<int, string>> GetCompanyDictionary()
        {
            var companies = await GetCompanies();

            var response = companies.ToDictionary(c => c.Id, c => c.CompanyName);
            return response;
        }

        public async Task DeleteCompany(int id)
        {
            var request = new RestRequest(companiesEndpoint, Method.DELETE);
            request.AddUrlSegment("id", id);

            var restResponse = await _client.ExecuteAsync(request);
            if (!restResponse.IsSuccessful)
            {
                throw new UmbracoServiceExeption(restResponse.StatusCode, restResponse.Content);
            }

            _cache.Remove("Companies");
            _cache.Remove($"Company{id}");

        }

        public async Task<IDictionary<int, string>> GetColorDictionary()
        {

            var colorsCacheKey = "Colors";
            Dictionary<int, string> response;
            if (!_cache.TryGetValue(colorsCacheKey, out response))
            {
                var request = new RestRequest(colorsEndpoint, Method.GET);
                request.AddUrlSegment("id", "");

                var restResponse = await _client.ExecuteAsync<Dictionary<int, string>>(request);
                if (restResponse.IsSuccessful)
                {
                    response = restResponse.Data;
                    _cache.Set(colorsCacheKey, response);
                }
                else
                {
                    throw new UmbracoServiceExeption(restResponse.StatusCode, restResponse.Content);
                }
            }

            return response;
        }

        public async Task<string> GetColorName(int id)
        {
            var dictionary = await GetColorDictionary();

            string response;
            if(!dictionary.TryGetValue(id, out response))
            {
                throw new UmbracoServiceExeption(HttpStatusCode.NotFound, $"Color with id={id} was not found");
            }

            return response;
        }

    }

}
