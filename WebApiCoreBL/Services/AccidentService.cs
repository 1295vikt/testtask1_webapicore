﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RTA_Project_BL.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApiCoreBL.Interfaces;
using WebApiCoreBL.ModelsBL;
using WebApiCoreDAL.Entities;

namespace WebApiCoreBL.Services
{
    public class AccidentService : GenericService<AccidentBL, Accident>, IAccidentService
    {

        public AccidentService(IGenericRepository<Accident> repository, IMapper mapper) : base(repository, mapper)
        {

        }

    }
}
