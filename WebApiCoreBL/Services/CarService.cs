﻿using AutoMapper;
using RTA_Project_BL.Services;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WebApiCoreBL.Interfaces;
using WebApiCoreBL.ModelsBL;
using WebApiCoreDAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace WebApiCoreBL.Services
{
    public class CarService : GenericService<CarBL, Car>, ICarService
    {

        public CarService(IGenericRepository<Car> repository, IMapper mapper) : base(repository, mapper)
        {
            
        }

        public async override Task<CarBL> Update(int id, CarBL modelBL)
        {
            var original = await _repository.FindById(id);

            modelBL.ManufactureYear = original.ManufactureYear;           

            return await base.Update(id, modelBL);
        }

        public async Task<IEnumerable<CarBL>> GetByCompanyId(int companyId)
        {
            var cars = await _repository.Query(c => c.CompanyId == companyId).Include(c => c.ImageLinks).ThenInclude(c => c.Image)
                .Include(c => c.Damages).Include(c => c.AccidentLinks).ThenInclude(a => a.Accident).ToListAsync();
            var result = Map(cars);
            return result;
        }

    }

}
