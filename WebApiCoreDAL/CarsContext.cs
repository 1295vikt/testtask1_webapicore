﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Text;
using WebApiCoreDAL.Entities;
using WebApiCoreDAL.Interfaces;

namespace WebApiCoreDAL
{
    public class CarsContext : DbContext
    {

        public CarsContext(DbContextOptions<CarsContext> options, ICarsDbInitializer initializer)
            : base(options)
        {
            initializer.Initialize(this);
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Accident>().HasMany(a=>a.Damages).WithOne(d=>d.Accident).HasForeignKey(d=>d.AccidentId).
                OnDelete(DeleteBehavior.SetNull);

            base.OnModelCreating(modelBuilder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }
    

        public DbSet<Car> Cars { get; set; }
        public DbSet<Accident> Accidents { get; set; }
        public DbSet<Damage> Damages { get; set; }
        public DbSet<Image> Images { get; set; }

        public DbSet<AccidentCar> AccidentCarLinks { get; set; }
        public DbSet<ImageCar> ImageCarLinks { get; set; }
        public DbSet<ImageAccident> ImageAccidentLinks { get; set; }
        public DbSet<ImageDamage> ImageDamageLinks { get; set; }

    }

}

