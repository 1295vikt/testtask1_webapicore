﻿using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Bogus;
using WebApiCoreDAL.Entities;
using WebApiCoreDAL.Interfaces;

namespace WebApiCoreDAL
{
    public class CarsDbInitializer : ICarsDbInitializer
    {
        public void Initialize(CarsContext context)
        {

            context.Database.EnsureCreated();

            if (!context.Cars.Any())
            {
                var faker = new Faker();

                var carBogus = new Faker<Car>()
                    .RuleFor(r => r.CompanyId, f => f.Random.Number(1,7))
                    .RuleFor(r => r.Model, f => f.Vehicle.Model())
                    .RuleFor(r => r.BodyType, f => f.Vehicle.Type())
                    .RuleFor(r => r.ColorId, f => f.Random.Number(1,87))
                    .RuleFor(r => r.GearboxTypeId, f => f.Random.Number(4))
                    .RuleFor(r => r.FuelType, f => f.Vehicle.Fuel())
                    .RuleFor(r => r.DriverName, f => f.Name.FullName())
                    .RuleFor(r => r.DriverContactPhone, f => f.Phone.PhoneNumber())
                    .RuleFor(r => r.ManufactureYear, f => f.Random.Number(2015, 2020))
                    .RuleFor(r => r.Mileage, f => f.Random.Number(500, 250000));


                var accidentBogus = new Faker<Accident>()
                    .RuleFor(r => r.AccidentAddress, f => f.Address.FullAddress())
                    .RuleFor(r => r.AccidentDate, f => f.Date.Past(8))
                    .RuleFor(r => r.Description, f => f.Lorem.Sentence());

                var damageBogus = new Faker<Damage>()
                    .RuleFor(r => r.DamageDescription, f => f.Lorem.Sentence())
                    .RuleFor(r => r.EstimatedRepairCostUSD, f => (f.Random.Number(40) + 1) * 100)
                    .RuleFor(r => r.IsRepaired, f => f.Random.Number(3) != 0 ? true : false);

                var imageBogus = new Faker<Image>()
                    .RuleFor(r => r.Description, f => f.Lorem.Sentence())
                    .RuleFor(r => r.ImageURL, f => f.Image.PicsumUrl(320,240));


                var cars = carBogus.Generate(faker.Random.Number(120, 200));
                foreach (var car in cars)
                {
                    var carImageLinks = new List<ImageCar>();

                    var carImages = imageBogus.Generate(faker.Random.Number(0, 5));
                    foreach (var carImage in carImages)
                    {
                        carImageLinks.Add(new ImageCar
                        {
                            Image = carImage
                        });
                    }

                    car.ImageLinks = carImageLinks;
                }

                context.Cars.AddRange(cars);
                context.SaveChanges();



                var accidents = accidentBogus.Generate(faker.Random.Number(25, 80));
                var carCount = context.Cars.Count();
                foreach (var accident in accidents)
                {
                    var carAccidentLinkList = new List<AccidentCar>();
                    var carDamageList = new List<Damage>();

                    int carsInAccident = 1;
                    if (faker.Random.Number(0, 3) == 0)
                    {
                        carsInAccident++;
                    }
                    if (faker.Random.Number(0, 4) == 0)
                    {
                        carsInAccident++;
                    }
                    if (faker.Random.Number(0, 5) == 0)
                    {
                        carsInAccident++;
                    }


                    var accidentImageLinks = new List<ImageAccident>();

                    var accidentImages = imageBogus.Generate(faker.Random.Number(0, 5));
                    foreach (var accidentImage in accidentImages)
                    {
                        accidentImageLinks.Add(new ImageAccident
                        {
                            Image = accidentImage
                        });
                    }
                    accident.ImageLinks = accidentImageLinks;


                    for (int i = 0; i < carsInAccident; i++)
                    {
                        int carId = faker.Random.Number(1, carCount);
                        var carAccidentLink = new AccidentCar()
                        {
                            CarId = carId
                        };

                        var carDamage = damageBogus.Generate();
                        carDamage.CarId = carId;

                        var damageImageLinks = new List<ImageDamage>();

                        var carDamageImages = imageBogus.Generate(faker.Random.Number(0, 4));
                        foreach (var damageImage in carDamageImages)
                        {
                            damageImageLinks.Add(new ImageDamage
                            {
                                Image = damageImage
                            });
                        }
                        carDamage.ImageLinks = damageImageLinks;

                        carAccidentLinkList.Add(carAccidentLink);
                        carDamageList.Add(carDamage);

                    }


                    accident.CarLinks = carAccidentLinkList;
                    accident.Damages = carDamageList;

                    context.Accidents.Add(accident);
                }

                context.SaveChanges();

            }
         

        }


    }
}
