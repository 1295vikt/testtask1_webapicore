﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiCoreDAL.Interfaces
{
    public interface ICarsDbInitializer
    {
        public void Initialize(CarsContext context);
    }
}
