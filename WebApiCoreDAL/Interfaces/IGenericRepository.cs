﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

public interface IGenericRepository<TEntity> where TEntity : class
{
    Task<TEntity> FindById(int id);

    Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> filter = null,
    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
    params Expression<Func<TEntity, object>>[] includes);

    Task<TEntity> GetFirstOrDefault(
    Expression<Func<TEntity, bool>> filter = null,
    params Expression<Func<TEntity, object>>[] includes);

    IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);

    Task<TEntity> Create(TEntity item);
    Task RemoveById(int id);
    Task<TEntity> Update(TEntity item);
    void Detatch(int id);
    void Dispose();
}