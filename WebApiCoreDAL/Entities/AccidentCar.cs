﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebApiCoreDAL.Entities
{
    public class AccidentCar
    {
        public int Id { get; set; }

        [ForeignKey("Accident")]
        public int AccidentId { get; set; }
        [ForeignKey("Car")]
        public int CarId { get; set; }

        public virtual Accident Accident { get; set; }
        public virtual Car Car { get; set; }
    }
}
