﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebApiCoreDAL.Entities
{
    public class Damage
    {
        public int Id { get; set; }
        [ForeignKey("Car")]
        public int CarId { get; set; }
        public int? AccidentId { get; set; }

        public string DamageDescription { get; set; }
        public int EstimatedRepairCostUSD { get; set; }
        public bool IsRepaired { get; set; }

        public virtual Car Car { get; set; }
        public virtual Accident Accident { get; set; }
        public virtual ICollection<ImageDamage> ImageLinks { get; set; }
    }
}
