﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebApiCoreDAL.Entities
{
    public class ImageDamage
    {
        public int Id { get; set; }

        [ForeignKey("Image")]
        public int ImageId { get; set; }
        [ForeignKey("CarDamage")]
        public int DamageId { get; set; }

        public virtual Image Image { get; set; }
        public virtual Damage Damage { get; set; }
    }
}
