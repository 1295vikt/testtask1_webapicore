﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace WebApiCoreDAL.Entities
{
    public class Accident
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string AccidentAddress { get; set; }
        public DateTime AccidentDate { get; set; }

        public virtual ICollection<AccidentCar> CarLinks { get; set; }
        public virtual ICollection<Damage> Damages { get; set; }
        public virtual ICollection<ImageAccident> ImageLinks { get; set; }

    }
}
