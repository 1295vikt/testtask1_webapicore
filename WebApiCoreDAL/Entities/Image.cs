﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiCoreDAL.Entities
{
    public class Image
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string ImageURL { get; set; }
    }
}
