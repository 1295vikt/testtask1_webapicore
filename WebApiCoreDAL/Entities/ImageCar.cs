﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebApiCoreDAL.Entities
{
    public class ImageCar
    {
        public int Id { get; set; }

        [ForeignKey("Image")]
        public int ImageId { get; set; }
        [ForeignKey("Car")]
        public int CarId { get; set; }

        public virtual Image Image { get; set; }
        public virtual Car Car { get; set; }
    }
}
