﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebApiCoreDAL.Entities
{
    public class ImageAccident
    {
        public int Id { get; set; }

        [ForeignKey("Image")]
        public int ImageId { get; set; }
        [ForeignKey("CarAccident")]
        public int AccidentId { get; set; }

        public virtual Image Image { get; set; }
        public virtual Accident Accident { get; set; }
    }
}
