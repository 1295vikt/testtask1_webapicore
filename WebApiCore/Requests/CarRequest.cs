﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Requests
{
    public class CarRequest
    {
        public int CompanyId { get; set; }
        [Required]
        [StringLength(30)]
        public string Model { get; set; }

        [Required]
        [StringLength(20)]
        public string BodyType { get; set; }

        [Required]
        [StringLength(20)]
        public string FuelType { get; set; }

        [Required]
        public int ColorId { get; set; }

        [Required]
        public int GearboxTypeId { get; set; }

        [Range(2000, 2020, ErrorMessage = "Invalid manufacture year")]
        public int ManufactureYear { get; set; }

        [Range(0, 2000000, ErrorMessage = "Invalid mileage")]
        public double Mileage { get; set; }

        [Required]
        [StringLength(50)]
        public string DriverName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})")]
        public string DriverContactPhone { get; set; }

        public IEnumerable<ImageRequest> Images { get; set; }
        public IEnumerable<DamageRequest> Damages { get; set; }
    }

}
