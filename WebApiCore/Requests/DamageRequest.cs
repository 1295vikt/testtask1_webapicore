﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Requests
{
    public class DamageRequest
    {
        [Required]
        public int CarId { get; set; }
        public string DamageDescription { get; set; }
        public int EstimatedRepairCostUSD { get; set; }
        public IEnumerable<ImageRequest> Images { get; set; }
    }
}
