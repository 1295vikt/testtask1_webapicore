﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Requests
{
    public class ImageRequest
    {
        public string Description { get; set; }
        [Required]
        public string ImageURL { get; set; }
    }
}
