﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Requests
{
    public class CompanyRequest
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string Comment { get; set; }
        public string LogoImageURL { get; set; }
    }

}
