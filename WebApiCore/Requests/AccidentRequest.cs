﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Requests
{
    public class AccidentRequest
    {
        [Required]
        public string Description { get; set; }
        public string AccidentAddress { get; set; }
        public DateTime AccidentDate { get; set; }
        public IEnumerable<ImageRequest> Images { get; set; }
        public IEnumerable<DamageRequest> Damages { get; set; }
    }
}
