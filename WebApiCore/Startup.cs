using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WebApiCoreBL.Services;
using WebApiCoreBL;
using WebApiCoreBL.Interfaces;
using AutoMapper;
using WebApiCoreBL.Configs;
using WebApiCore.Configs;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using WebApiCore.Exceptions;
using Microsoft.AspNetCore.Http;
using System.Net.Mime;

namespace TestTask1_WebApiCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            UmbracoApiConfiguration apiConfiguration = new UmbracoApiConfiguration();
            Configuration.GetSection("UmbracoApiConfiguration").Bind(apiConfiguration);
            services.AddSingleton(apiConfiguration);

            services.AddSingleton(Configuration);

            BLServices.AddServices(services, Configuration);

            services.AddTransient(typeof(ICarService), typeof(CarService));
            services.AddTransient(typeof(IUmbracoService), typeof(UmbracoService));
            services.AddTransient(typeof(IAccidentService), typeof(AccidentService));

            services.AddMemoryCache();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new WebAutomapperProfile());
                mc.AddProfile(new BLAutomapperProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins", builder =>
                 builder.AllowAnyOrigin()
                 .AllowAnyHeader()
                 .AllowAnyMethod());
            });


            services.AddControllers(options => options.Filters.Add(new HttpResponseExceptionFilter()));

            services.AddControllers()
                .ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var result = new BadRequestObjectResult(context.ModelState);

                    result.ContentTypes.Add(MediaTypeNames.Application.Json);
                    result.ContentTypes.Add(MediaTypeNames.Application.Xml);

                    return result;
                };
            }); ;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IMapper mapper, IUmbracoService umbracoService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors("AllowAllOrigins");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

        }


    }
}
