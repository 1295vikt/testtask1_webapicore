﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApiCore.Requests;
using WebApiCore.Responses;
using WebApiCoreBL.ModelsBL;
using System.Drawing;
using WebApiCoreBL.Interfaces;
using System.Threading.Tasks;
using WebApiCoreBL;

namespace WebApiCore.Configs
{
    public class WebAutomapperProfile : Profile
    {

        public WebAutomapperProfile()
        {

            CreateMap<CarBL, CarResponse>();
            CreateMap<CarBL, CarResponseShort>()
                .AfterMap((src, dest) =>
                {
                    if (src.Images.Any())
                    {
                        dest.ImageURL = src.Images.First().ImageURL;
                    }
                }
                );

            CreateMap<AccidentBL, AccidentResponse>();
            CreateMap<AccidentBL, AccidentResponseShort>();

            CreateMap<DamageBL, DamageResponse>();
            CreateMap<ImageBL, ImageResponse>();
            CreateMap<CarRequest, CarBL>();
            CreateMap<AccidentRequest, AccidentBL>();
            CreateMap<DamageRequest, DamageBL>();
            CreateMap<ImageRequest, ImageBL>();

            CreateMap<CompanyRequest, UmbracoCompanyRequest>().ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Name));

            CreateMap<UmbracoCompanyResponse, CompanyResponse>().ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CompanyName));
            CreateMap<UmbracoCompanyResponse, CompanyResponseShort>().ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CompanyName));

        }

    }
}
