﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Responses
{
    public class CarResponse
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Model { get; set; }
        public string BodyType { get; set; }
        public string FuelType { get; set; }
        public string Color { get; set; }
        public int GearboxTypeId { get; set; }
        public int ManufactureYear { get; set; }
        public double Mileage { get; set; }
        public string DriverName { get; set; }
        public string DriverContactPhone { get; set; }
        public CompanyResponseShort Company { get; set; }
        public IEnumerable<ImageResponse> Images { get; set; }
        public IEnumerable<DamageResponse> Damages { get; set; }
    }

    public class CarResponseShort
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string DriverName { get; set; }
        public string DriverContactPhone { get; set; }
        public string ImageURL { get; set; }
    }

    public class CarResponsePaged
    {
        public IEnumerable<CarResponseShort> Cars { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }

    }
}

