﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Responses
{
    public class ImageResponse
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string ImageURL { get; set; }
    }

}
