﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Responses
{
    public class DamageResponse
    {
        public int Id { get; set; }
        public int CarId { get; set; }
        public int? AccidentId { get; set; }
        public string DamageDescription { get; set; }
        public int EstimatedRepairCostUSD { get; set; }
        public bool IsRepaired { get; set; }
        public IEnumerable<ImageResponse> Images { get; set; }
    }

}
