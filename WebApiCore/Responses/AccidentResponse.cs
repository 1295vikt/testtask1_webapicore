﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Responses
{
    //GET responses
    public class AccidentResponse
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string AccidentAddress { get; set; }
        public DateTime AccidentDate { get; set; }
        public IEnumerable<ImageResponse> Images { get; set; }
        public IEnumerable<DamageResponse> Damages { get; set; }
    }

    public class AccidentResponseShort
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string AccidentAddress { get; set; }
        public DateTime AccidentDate { get; set; }
    }

}
