﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Responses
{

    public class CompanyResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string Comment { get; set; }
        public string LogoImageURL { get; set; }
        public IEnumerable<CarResponseShort> Cars { get; set; } 

    }

    public class CompanyResponseShort
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string LogoImageURL { get; set; }
    }

}
