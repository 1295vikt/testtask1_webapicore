﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using WebApiCore.Requests;
using Microsoft.AspNetCore.Mvc;
using WebApiCore.Responses;
using WebApiCoreBL.Interfaces;
using WebApiCoreBL.ModelsBL;
using System.Net;
using WebApiCore.Exceptions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiCore.Controllers
{
    [Route("api/accidents")]
    [ApiController]
    public class AccidentsController : ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly IAccidentService _accidentService;
        private readonly ICarService _carService;

        public AccidentsController(IMapper mapper, IAccidentService accidentService, ICarService carService)
        {
            _mapper = mapper;
            _accidentService = accidentService;
            _carService = carService;
        }

        // GET: api/<AccidentsController>
        [HttpGet]
        public async Task<IEnumerable<AccidentResponseShort>> Get()
        {
            var accidents = await _accidentService.GetAll();

            var response = _mapper.Map<IEnumerable<AccidentResponseShort>>(accidents);
            return response;
        }

        // GET api/<AccidentsController>/5
        [HttpGet("{id}")]
        public async Task<AccidentResponse> Get(int id)
        {

            var accident = await _accidentService.FindById(id);

            if (accident == null)
                throw new HttpResponseException(HttpStatusCode.NotFound, "Accident not found");

            var response = _mapper.Map<AccidentResponse>(accident);
            return response;

        }

        //POST api/<AccidentsController>
        [HttpPost]
        public async Task<AccidentResponse> Post([FromBody] AccidentRequest accident)
        {
            foreach (var damage in accident.Damages)
            {
                if (!await _carService.CheckIfExists(damage.CarId))
                    throw new HttpResponseException(HttpStatusCode.NotFound, "One or more damaged cars not found");
            }

            var accidentBL = _mapper.Map<AccidentBL>(accident);
            accidentBL = await _accidentService.Create(accidentBL);

            var response = _mapper.Map<AccidentResponse>(accidentBL);
            return response;
        }

        // PUT api/<AccidentsController>/5
        [HttpPut("{id}")]
        public async Task<AccidentResponse> Put(int id, [FromBody] AccidentRequest accident)
        {
            if (!await _accidentService.CheckIfExists(id))
                throw new HttpResponseException(HttpStatusCode.BadRequest, "Accident not found");

            foreach (var damage in accident.Damages)
            {
                if (!await _carService.CheckIfExists(damage.CarId))
                    throw new HttpResponseException(HttpStatusCode.NotFound, "One or more damaged cars not found");
            }

            var accidentBL = _mapper.Map<AccidentBL>(accident);
            accidentBL.Id = id;
            accidentBL = await _accidentService.Update(id, accidentBL);

            var response = _mapper.Map<AccidentResponse>(accidentBL);
            return response;
        }

        // DELETE api/<AccidentsController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            if (!await _accidentService.CheckIfExists(id))
                throw new HttpResponseException(HttpStatusCode.NotFound, "Accident not found");

            await _accidentService.Delete(id);
        }
    }
}
