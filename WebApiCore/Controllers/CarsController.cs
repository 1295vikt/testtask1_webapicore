﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using WebApiCore.Exceptions;
using WebApiCore.Requests;
using WebApiCore.Responses;
using WebApiCoreBL.Interfaces;
using WebApiCoreBL.ModelsBL;
using Microsoft.Extensions.Caching.Memory;
using WebApiCoreBL;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using System.Linq.Expressions;

namespace WebApiCore.Controllers
{
    [Route("api/cars")]
    [ApiController]
    public class CarsController : ControllerBase
    {

        private readonly ICarService _carService;
        private readonly IUmbracoService _umbracoService;
        private readonly IMapper _mapper;

        public CarsController(IMapper mapper, ICarService carService, IUmbracoService umbracoService)
        {
            _carService = carService;
            _umbracoService = umbracoService;
            _mapper = mapper;
        }

        // GET: api/<CarsController>
        [HttpGet]
        public async Task<IEnumerable<CarResponseShort>> Get()
        {

            var cars = await _carService.GetAll();

            var colorDictionary = await _umbracoService.GetColorDictionary();

            var companyDictionary = await _umbracoService.GetCompanyDictionary();

            var response = _mapper.MapCarListShort(cars, colorDictionary, companyDictionary);

            return response;
        }

        [HttpGet]
        [Route("page")]
        public async Task<CarResponsePaged> Get(int itemsPerPage, int currentPage, [FromQuery] int? companyId = null)
        {
            if (currentPage < 1 || itemsPerPage < 1)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest, "Page and item count cannot be less than 1");
            }

            var filters = new List<Expression<Func<CarBL, bool>>>();
            if (companyId != null)
            {
                Expression<Func<CarBL, bool>> companyFilter = c => c.CompanyId == companyId;
                filters.Add(companyFilter);
            }

            var carsPaged = await _carService.GetPaged(itemsPerPage, currentPage, filters.ToArray());

            var colorDictionary = await _umbracoService.GetColorDictionary();

            var companyDictionary = await _umbracoService.GetCompanyDictionary();

            var response = new CarResponsePaged
            {
                Cars = _mapper.MapCarListShort(carsPaged.Items, colorDictionary, companyDictionary),
                CurrentPage = carsPaged.CurrentPage,
                TotalPages = carsPaged.TotalPages
            };

            return response;
        }

        // GET api/<CarsController>/5
        [HttpGet("{id}")]
        public async Task<CarResponse> Get(int id, [FromQuery] bool includecompany = false)
        {
            var car = await _carService.FindById(id);

            if (car == null)
                throw new HttpResponseException(HttpStatusCode.NotFound, "Car not found");

            var colorDictionary = await _umbracoService.GetColorDictionary();
            var companyDictionary = await _umbracoService.GetCompanyDictionary();

            var response = _mapper.MapCar(car, colorDictionary, companyDictionary);

            if (includecompany)
            {
                try
                {
                    var company = _umbracoService.GetCompanyById(car.CompanyId);
                    response.Company = _mapper.Map<CompanyResponseShort>(company);
                }
                catch (UmbracoServiceExeption ex)
                {
                    throw new HttpResponseException(ex.StatusCode, ex.Message);
                }
            }

            return response;
        }

        // POST api/<CarsController>
        [HttpPost]
        public async Task<CarResponse> Post([FromBody] CarRequest car)
        {

            var carBL = _mapper.Map<CarBL>(car);
            carBL = await _carService.Create(carBL);

            var response = _mapper.Map<CarResponse>(carBL);
            return response;
        }

        // PUT api/<CarsController>/5
        [HttpPut("{id}")]
        public async Task<CarResponse> Put(int id, [FromBody] CarRequest car)
        {
            if (!await _carService.CheckIfExists(id))
                throw new HttpResponseException(HttpStatusCode.NotFound, "Car not found");

            var carBL = _mapper.Map<CarBL>(car);
            carBL.Id = id;
            carBL = await _carService.Update(id, carBL);

            var response = _mapper.Map<CarResponse>(carBL);
            return response;
        }

        // DELETE api/<CarsController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            if (!await _carService.CheckIfExists(id))
                throw new HttpResponseException(HttpStatusCode.NotFound, "Car not found");

            await _carService.Delete(id);
        }

    }
}
