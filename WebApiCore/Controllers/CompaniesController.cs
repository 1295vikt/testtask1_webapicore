﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using WebApiCore.Exceptions;
using WebApiCore.Requests;
using WebApiCore.Responses;
using WebApiCoreBL;
using WebApiCoreBL.Interfaces;
using WebApiCoreBL.ModelsBL;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiCore.Controllers
{
    [Route("api/companies")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUmbracoService _umbracoService;
        private readonly ICarService _carService;

        public CompaniesController(IMapper mapper, IUmbracoService umbracoService, ICarService carService)
        {
            _mapper = mapper;
            _umbracoService = umbracoService;
            _carService = carService;
        }

        // GET: api/<CompaniesController>
        [HttpGet]
        public async Task<IEnumerable<CompanyResponseShort>> Get()
        {
            try
            {
                var companies = await _umbracoService.GetCompanies();

                var response = _mapper.Map<IEnumerable<CompanyResponseShort>>(companies);

                return response;
            }
            catch (UmbracoServiceExeption ex)
            {
                throw new HttpResponseException(ex.StatusCode, ex.Message);
            }

        }

        // GET: api/<CompaniesController>/5
        [HttpGet("{id}")]
        public async Task<CompanyResponse> Get(int id, [FromQuery] bool includecars = false)
        {
            try
            {
                var company = await _umbracoService.GetCompanyById(id);

                var response = _mapper.Map<CompanyResponse>(company);

                if (includecars)
                {
                    var cars = await _carService.GetByCompanyId(id);
                    var colorDictionary = await _umbracoService.GetColorDictionary();
                    response.Cars = _mapper.MapCarListShort(cars, colorDictionary);

                }
                return response;
            }
            catch (UmbracoServiceExeption ex)
            {
                throw new HttpResponseException(ex.StatusCode, ex.Message);
            }
        }

        [HttpGet]
        [Route("dictionary")]
        public async Task<IEnumerable<KeyValuePair<int,string>>> GetDictionary()
        {
            try
            {
                var dictionary = await _umbracoService.GetCompanyDictionary();

                //var response = dictionary.AsEnumerable();
                var response = dictionary.Select(d => new KeyValuePair<int, string>(d.Key, d.Value));

                return response;
            }
            catch (UmbracoServiceExeption ex)
            {
                throw new HttpResponseException(ex.StatusCode, ex.Message);
            }
        }

        // POST api/<CompaniesController>
        [HttpPost]
        public async Task<CompanyResponse> Post([FromBody] CompanyRequest companyRequest)
        {
            try
            {
                var umbracoRequest = _mapper.Map<UmbracoCompanyRequest>(companyRequest);

                var company = await _umbracoService.CreateCompany(umbracoRequest);

                var response = _mapper.Map<CompanyResponse>(company);

                return response;
            }
            catch (UmbracoServiceExeption ex)
            {
                throw new HttpResponseException(ex.StatusCode, ex.Message);
            }
        }

        //PUT api/<CompaniesController>/5
        [HttpPut("{id}")]
        public async Task<CompanyResponse> Put(int id, [FromBody] CompanyRequest companyRequest)
        {
            try
            {
                var umbracoRequest = _mapper.Map<UmbracoCompanyRequest>(companyRequest);

                var company = await _umbracoService.UpdateCompany(id, umbracoRequest);

                var response = _mapper.Map<CompanyResponse>(company);

                return response;
            }
            catch (UmbracoServiceExeption ex)
            {
                throw new HttpResponseException(ex.StatusCode, ex.Message);
            }
        }

        // DELETE api/<CompaniesController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            try
            {
                await _umbracoService.DeleteCompany(id);
            }
            catch (UmbracoServiceExeption ex)
            {
                throw new HttpResponseException(ex.StatusCode, ex.Message);
            }
        }

    }
}
