﻿using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiCore.Responses;
using WebApiCoreBL.ModelsBL;

namespace WebApiCore
{
    public static class MapperExtensions
    {
        public static CarResponse MapCar(this IMapper mapper, CarBL car,
            IDictionary<int, string> colorDictionary, IDictionary<int, string> companyDictionary = null)
        {
            return mapper.Map<CarBL, CarResponse>(car, opt => opt.AfterMap((src, dest) =>
            {
                var colorId = src.ColorId;
                string colorName;
                if (colorDictionary.TryGetValue(colorId, out colorName))
                {
                    dest.Color = colorName;
                }

                if (companyDictionary != null)
                {
                    var companyId = src.CompanyId;
                    string companyName;
                    if (companyDictionary.TryGetValue(companyId, out companyName))
                    {
                        dest.CompanyName = companyName;
                    }
                }

            }));
        }

        public static IEnumerable<CarResponseShort> MapCarListShort(this IMapper mapper, IEnumerable<CarBL> cars,
            IDictionary<int, string> colorDictionary, IDictionary<int, string> companyDictionary = null)
        {
            return cars.Select(c => mapper.Map<CarBL, CarResponseShort>(c, opt => opt.AfterMap((src, dest) =>
            {
                var colorId = src.ColorId;
                string colorName;
                if (colorDictionary.TryGetValue(colorId, out colorName))
                {
                    dest.Color = colorName;
                }

                if (companyDictionary != null)
                {
                    var companyId = src.CompanyId;
                    string companyName;
                    if (companyDictionary.TryGetValue(companyId, out companyName))
                    {
                        dest.CompanyName = companyName;
                    }
                }
            })));
        }

    }
}
